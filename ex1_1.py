from chartables.chars import * 

fname1='inputfiles/ex1_1_hex.txt'
fname2='inputfiles/ex1_1_b64.txt'

def ex1(fl1,fl2,lng,a,b):
  f1 = open(fl1)
  binstr = ''
  line = f1.readline()
  for char in line.rstrip():
    binstr += a[char]
  f1.close()

  buf = '' 
  retstr = ''
  for char in binstr+' ': #equivelant of saying binstr+1
    if len(buf) == lng:
      retstr+=b[buf]
      buf = char
    else:
      buf += char

  f2 = open(fl2)
  line = f2.readline()
  f2.close()

  return retstr == line.rstrip()


print ex1(fname1,fname2,6,HEXb,B64v)
print ex1(fname2,fname1,4,B64b,HEXv)
    
