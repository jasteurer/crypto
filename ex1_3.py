from chartables.chars import *
from chartables.funcs import *

fname = 'inputfiles/ex1_3.txt'

ln = getlines(fname)[0]
#struct to hold score, string, key
#modified iff score is higher 
#cheaper than building/maintaining a dict

ret = ['','',0]
for k in ASCb.keys():
  s = bindecode(xor(binencode(ln,HEXb),ASCb[k]),ASCv,8)
  i = score(s)
  if i > ret[2]:
    ret[0]=s
    ret[1]=k
    ret[2]=i
  elif i == ret[2]:
    raise cerr('encountered equal score. something wrong with scoring')

print '{str}:',ret[0],'{key}:',ret[1]
