from chars import ASCb

class cerr(Exception):
  def __init__(self, value):
    self.value = value
  def __str__(self):
    return repr(self.value)

def binencode(nstr, ntbl):
  ret=''
  for char in nstr:
    ret+=ntbl[char]
  return ret  

def bindecode(nstr, ntbl, sz):
  ret=''
  buf=''
  for char in nstr+' ':
    if len(buf) == sz:
      ret +=ntbl[buf]
      buf = char
    else:
      buf += char
  return ret 


def xor(a,b):
  if len(a) != len(b):
    if len(b) > len(a):
      a,b=b,a
    ct=len(b)
    for i in range(ct,len(a)):
      b+=b[i%ct]
  ret=''
  xtbl={('0','0'):'0',('0','1'):'1',('1','0'):'1',('1','1'):'0'}
  for cl in zip(a,b):
    ret += xtbl[cl]  
  return ret  

def getlines(fname):
  ret=[]
  f = open(fname,'r')
  for line in f:
    ret.append(line.rstrip())
  f.close()
  return ret

#odd escape chars
scrdict = dict.fromkeys(range(1,7)+range(14,27)+range(28,32)+[127],-2)
#typical escape characters
scrdict.update(dict.fromkeys([0]+range(7,14)+[27],0))
#ascii symbols
scrdict.update(dict.fromkeys(range(35,39)+range(40,44)+[47]+range(58,63)+[64]+range(91,97)+range(123,127),-1))
#common sentence punctuation
scrdict.update(dict.fromkeys(range(32,35)+[39]+range(44,47)+[63],1))
#numbers 
scrdict.update(dict.fromkeys(range(48,58),1))
#upper
scrdict.update(dict.fromkeys(range(65,91),2))
#lower
scrdict.update(dict.fromkeys(range(97,123),3))

def score(nstr):
  score = 0
  for char in nstr:
    score+=scrdict[int(ASCb[char],2)]
  return score
