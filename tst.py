#escape chars
scrdict = dict.fromkeys(range(0,31)+[127],-2)
#ascii symbols
scrdict.update(dict.fromkeys(range(35,39)+range(40,44)+[47]+range(58,63)+[64]+range(91,97)+range(123,127),-1))
#common sentence punctuation
scrdict.update(dict.fromkeys(range(32,35)+[39]+range(44,47)+[63],1))
#numbers 
scrdict.update(dict.fromkeys(range(48,58),1))
#upper
scrdict.update(dict.fromkeys(range(65,91),2))
#lower
scrdict.update(dict.fromkeys(range(97,123),3))
