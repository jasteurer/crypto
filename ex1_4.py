from chartables.chars import *
from chartables.funcs import *

fname = 'inputfiles/ex1_4.txt'

lns = getlines(fname)
ct=0
ret = ['','',0]
for ln in lns:
  bln = binencode(ln,HEXb)
  for k in ASCb.keys(): 
    try:
      s = bindecode(xor(bln,ASCb[k]),ASCv,8)
      n = score(s)
      if n > ret[2]:
        ret[0]=k
        ret[1]=s
        ret[2]=n
    except KeyError:
      ct+=1

print ct, ret
