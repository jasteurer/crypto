from chartables.chars import *
from chartables.funcs import *

fname = 'inputfiles/ex1_2.txt'

f = open(fname,'r')

a=f.readline().rstrip()
b=f.readline().rstrip()
x=f.readline().rstrip()

print 'Exercise 2 is valid: ',bindecode(xor(binencode(a,HEXb),binencode(b,HEXb)),HEXv,4) == x

f.close()
